//
//  SVDayWeather.m
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 23.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVDayWeather.h"
#import "SVHelper.h"

@implementation SVDayWeather

#pragma mark - Initialization

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        NSDictionary *temperatureDictionary = dictionary[SVHelperTemperature];
        _minTemperature = temperatureDictionary[SVHelperMinTemperature];
        _maxTemperature = temperatureDictionary[SVHelperMaxTemperature];
        _eveTemperature = temperatureDictionary[SVHelperEveTemperature];
        _dayTemperature = temperatureDictionary[SVHelperDayTemperature];
        _morningTemperature = temperatureDictionary[SVHelperMorningTemperature];
        _nightTemperature = temperatureDictionary[SVHelperNight];
    }
    return self;
}

@end

//
//  SVApiManager.h
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 23.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SVHelper.h"

@interface SVAPIManager : NSObject

#pragma mark - Initialization

+ (instancetype)sharedManager;

#pragma mark - Methods

- (void)getLast16DaysMinskWeatherWithCompletion:(SucсessfulBlock)completion;

@end

//
//  SVStaticsView.h
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface SVStaticsView : UIView

@property (nonatomic, strong) NSNumber *minValue;
@property (nonatomic, strong) NSNumber *maxValue;

@property (nonatomic, strong) NSMutableArray *numbersToShow;
@property (nonatomic, strong) NSNumber *numberToShow;

#pragma mark - Markers

- (void)addNumberToShow:(NSNumber *)number;

#pragma mark - Animation


@end

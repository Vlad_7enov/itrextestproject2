//
//  SVApiManager.m
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 23.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVAPIManager.h"

static NSString *HistoryByCoordinatesURL = @"http://api.openweathermap.org/data/2.5/forecast/daily?q=Minsk&mode=json&units=metric&cnt=16&appid=b4474caa975a1f5b5c4f8362b59a31b8";

@implementation SVAPIManager

#pragma mark - Initialization

+ (instancetype)sharedManager {
    static SVAPIManager* _manager = NULL;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _manager = [[SVAPIManager alloc] init];
    });
    
    return _manager;
}

#pragma mark - Methods

- (void)getLast16DaysMinskWeatherWithCompletion:(SucсessfulBlock)completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURL *URL = [NSURL URLWithString:HistoryByCoordinatesURL];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:URL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error];
            completion(nil, json);
        }
        else {
            completion(@"Check internet connection", nil);
        }
    }];
    [dataTask resume];
}

@end

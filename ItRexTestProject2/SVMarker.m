//
//  SVMarker.m
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 26.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVMarker.h"

CGFloat ArrowHeight = 30;
CGFloat Offset = 5;

@interface SVMarker ()

@property (nonatomic) CGPoint circleCenter;

@end

@implementation SVMarker

#pragma mark - Initialization

- (instancetype)initWithValue:(NSString *)value andFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _value = value;
    }
    return self;
}

#pragma mark - Custom drawing

- (void)drawRect:(CGRect)rect {
    [[UIColor whiteColor] setFill];
    CGPoint markerCenter = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height * 0.4);
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:markerCenter radius:self.bounds.size.width / 2 * 0.6 startAngle:M_PI / 4 endAngle:(M_PI - M_PI / 4) clockwise:NO];
    [path addLineToPoint:CGPointMake(markerCenter.x, self.bounds.size.height)];
    [path closePath];
    [path fill];
    [path stroke];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    NSString *stringToDraw = @"11";
    [self.value drawAtPoint:CGPointMake(10, 6) withAttributes:nil];
}

@end

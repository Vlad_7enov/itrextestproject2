//
//  SVMarker.h
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 26.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface SVMarker : UIView

@property (nonatomic, strong) NSString *value;

#pragma mark - Initialization

- (instancetype)initWithValue:(NSString *)value andFrame:(CGRect)frame;

@end

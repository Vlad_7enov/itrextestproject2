//
//  SVStatiscticsViewController.m
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "SVStatiscticsViewController.h"
#import "SVStaticsView.h"
#import "SVAPIManager.h"
#import "SVDayWeather.h"

@interface SVStatiscticsViewController () <CLLocationManagerDelegate>

@property (nonatomic ,strong) NSMutableArray *weatherStatistics;
@property (nonatomic, weak) IBOutlet SVStaticsView *statisticsView;

@end

@implementation SVStatiscticsViewController

#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.weatherStatistics = [[NSMutableArray alloc] init];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self loadHistory];
}

#pragma mark - Update view

- (void)updateView {
    SVDayWeather *firstDay = self.weatherStatistics[self.weatherStatistics.count - 2];
    self.statisticsView.minValue = firstDay.minTemperature;
    self.statisticsView.maxValue = firstDay.maxTemperature;
    [self.statisticsView addNumberToShow:firstDay.eveTemperature];
    [self.statisticsView addNumberToShow:firstDay.dayTemperature];
    [self.statisticsView addNumberToShow:firstDay.morningTemperature];
    [self.statisticsView addNumberToShow:firstDay.nightTemperature];
}

#pragma mark - Load history

- (void)loadHistory {
    [[SVAPIManager sharedManager] getLast16DaysMinskWeatherWithCompletion:^(NSString *error, id responseObject) {
        if (responseObject) {
            for (NSDictionary *currentDay in responseObject[SVHelperList]) {
                SVDayWeather *currentDayWeather = [[SVDayWeather alloc] initWithDictionary:currentDay];
                [self.weatherStatistics addObject:currentDayWeather];
            }
            [self updateView];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:error preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    }];
}

@end

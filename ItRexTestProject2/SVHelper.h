//
//  SVHelper.h
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 23.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <Foundation/Foundation.h>

//Blocks
typedef void(^SucсessfulBlock)(NSString* error, id responseObject);

//JSON keys
static NSString *SVHelperTemperature = @"temp";
static NSString *SVHelperMaxTemperature = @"max";
static NSString *SVHelperMinTemperature = @"min";
static NSString *SVHelperEveTemperature = @"eve";
static NSString *SVHelperDayTemperature = @"day";
static NSString *SVHelperMorningTemperature = @"morn";
static NSString *SVHelperList = @"list";
static NSString *SVHelperNight = @"night";

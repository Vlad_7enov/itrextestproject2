//
//  SVDayWeather.h
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 23.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVDayWeather : NSObject

@property (nonatomic, strong) NSNumber *minTemperature;
@property (nonatomic, strong) NSNumber *maxTemperature;
@property (nonatomic, strong) NSNumber *eveTemperature;
@property (nonatomic, strong) NSNumber *dayTemperature;
@property (nonatomic, strong) NSNumber *morningTemperature;
@property (nonatomic, strong) NSNumber *nightTemperature;

#pragma mark - Initialization

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

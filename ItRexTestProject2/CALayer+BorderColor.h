//
//  CALayer+BorderColor.h
//  SnappySweeps
//
//  Created by Vladsilav Semenov on 23.04.16.
//  Copyright © 2016 Balina Soft. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer (BorderColor)

@property (nonatomic, strong) UIColor *borderUIColor;

@end

//
//  SVStaticsView.m
//  ItRexTestProject2
//
//  Created by Vladsilav Semenov on 22.06.16.
//  Copyright © 2016 Vladislav Semenov. All rights reserved.
//

#import "SVStaticsView.h"
#import "SVMarker.h"

CGFloat MinValue = 0;
CGFloat MaxValue = 100;
CGFloat OutLineOffset = 50;
CGFloat LineWidth = 30;

CGFloat MarkerHeight = 33;
CGFloat MarkerWidth = 33;

static NSString *AnimationKeyPosition = @"position";
static NSString *AnimationKeyRotation = @"transform.rotation.z";

@interface SVStaticsView ()

@property (nonatomic) CGFloat animationDelay;
@property (nonatomic, strong) NSMutableArray *markers;
@property (nonatomic) CGFloat radius;
@property (nonatomic) CGPoint halfCircleCenter;

@end

@implementation SVStaticsView

#pragma mark - Accessors

- (void)setMaxValue:(NSNumber *)maxValue {
    
    _maxValue = maxValue;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}

- (void)setMinValue:(NSNumber *)minValue {
    _minValue = minValue;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}

#pragma mark - Markers

- (void)addNumberToShow:(NSNumber *)number {
    
    NSString *markerValue = [NSString stringWithFormat:@"%lu",number.integerValue];
    
    CGPoint center = CGPointMake(self.frame.size.width / 2, self.bounds.size.height - OutLineOffset);
    
    SVMarker *currentMarker = [[SVMarker alloc] initWithValue:markerValue andFrame:CGRectMake(5, center.y, MarkerWidth, MarkerHeight)];
    currentMarker.backgroundColor = [UIColor clearColor];
    currentMarker.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [currentMarker setNeedsDisplay];
        [self addSubview:currentMarker];
        
        [self animateMarker:currentMarker toValue:number];
    });
}

#pragma mark - Custom drawing

- (void)drawRect:(CGRect)rect {
    [[UIColor blackColor] setStroke];
    [[UIColor lightGrayColor] setFill];
    
    if (self.minValue && self.maxValue) {
        NSString *minValue = self.minValue.stringValue;
        NSString *maxValue = self.maxValue.stringValue;
        
        NSDictionary *textAttributes = @{
                                         NSFontAttributeName : [UIFont fontWithName:@"Helvetica Neue" size:20]
                                         };
        CGFloat textFrameSize = 60;
        [minValue drawInRect:CGRectMake(OutLineOffset - 10, self.halfCircleCenter.y + LineWidth - 10, textFrameSize, textFrameSize) withAttributes:textAttributes];
        [maxValue drawInRect:CGRectMake(self.frame.size.width - (OutLineOffset - 20) - textFrameSize, self.halfCircleCenter.y + LineWidth - 10, textFrameSize, textFrameSize) withAttributes:textAttributes];
    }
    
    //Draw in line
    self.radius = self.frame.size.width / 2 - OutLineOffset;
    
    CGPoint point = CGPointMake(OutLineOffset, self.bounds.size.height - OutLineOffset);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = 2.0;
    [path addArcWithCenter:CGPointMake(point.x + LineWidth / 2, point.y) radius:LineWidth / 2 startAngle:M_PI endAngle:M_PI * 2 clockwise:NO];
    
    point.x += self.radius;
    self.halfCircleCenter = point;
    [path addArcWithCenter:point radius:self.radius startAngle:M_PI endAngle:M_PI * 2 clockwise:YES];
    
    //Draw line beetween in and out
    [path addArcWithCenter:CGPointMake(self.frame.size.width - (OutLineOffset + LineWidth / 2), point.y) radius:LineWidth / 2 startAngle:M_PI endAngle:2 * M_PI clockwise:NO];
    
    //Draw in line
    point.x -= self.radius - LineWidth;
    [path addArcWithCenter:self.halfCircleCenter radius:self.radius - LineWidth startAngle:0 endAngle:M_PI clockwise:NO];
    
    [path stroke];
    [path addClip];
    
    [self drawGradient];
}

#pragma mark - Gradient

- (void)drawGradient {
    NSArray* gradientColors = [NSArray arrayWithObjects:
                               (id)[UIColor redColor].CGColor,
                               (id)[UIColor orangeColor].CGColor,
                               (id)[UIColor yellowColor].CGColor,
                               (id)[UIColor greenColor].CGColor,
                               (id)[UIColor blueColor].CGColor,
                               (id)[UIColor purpleColor].CGColor, nil];
    
    NSInteger colorIndex = 0;
    CGFloat stepAngle = M_PI / 5;
    CGFloat startAngle = M_PI - M_PI / 20;
    BOOL isAllDone = NO;
    while (!isAllDone) {
        NSInteger nextColorIndex = (colorIndex + 1 < gradientColors.count) ? colorIndex + 1: colorIndex ;
        NSArray *colors = @[gradientColors[colorIndex], gradientColors[nextColorIndex]];
        [self drawGradiendWithColors:colors fromAngle:startAngle toAngle:startAngle + stepAngle];
        startAngle += stepAngle;
        if (colorIndex + 1 < gradientColors.count) {
            colorIndex++;
        }
        else {
            isAllDone = YES;
        }
    }
}

- (void)drawGradiendWithColors:(NSArray *)colors fromAngle:(CGFloat)startAngle toAngle:(CGFloat)endAngle {
    CGPoint center = self.halfCircleCenter;
    CGFloat radius = self.radius;
    
    CGContextSaveGState(UIGraphicsGetCurrentContext());
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:center];
    [path addArcWithCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
    [path addClip];
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(center.x - sinf(startAngle - M_PI_2) * radius, center.y + cosf(startAngle - M_PI_2) * radius);
    CGPoint endPoint = CGPointMake(center.x - sinf(endAngle - M_PI_2) * radius, center.y + cosf(endAngle - M_PI_2) * radius);
    CGContextDrawLinearGradient(UIGraphicsGetCurrentContext(), gradient, startPoint, endPoint, kCGGradientDrawsAfterEndLocation|kCGGradientDrawsBeforeStartLocation);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
}

#pragma mark - Angle calculations

- (CGFloat)angleBetweenMinAndNumber:(NSNumber *)numberToShow {
    
    if ([self.maxValue isEqual:self.minValue]) {
        return 0;
    }
    
    CGFloat arcFromStartAngle = (numberToShow.floatValue - self.minValue.floatValue) / ((self.maxValue.floatValue - self.minValue.floatValue) / M_PI);
    
    return arcFromStartAngle;
}

#pragma mark - Animation

- (void)animateMarker:(SVMarker *)marker toValue:(NSNumber *)number {
    CGFloat angle = [self angleBetweenMinAndNumber:number];
    
    //Animation duration
    CGFloat defaultAnimationDuration = 5.0;//For angle = M_PI
    CGFloat animationSpeed = defaultAnimationDuration / M_PI;//secs for rad
    CGFloat animationDuration = animationSpeed * angle;
    
    //Rotation animation
    CABasicAnimation *markerRotationAnimation = [CABasicAnimation animationWithKeyPath:AnimationKeyRotation];
    markerRotationAnimation.fromValue = @(3 * M_PI_2);
    markerRotationAnimation.toValue = @(3 * M_PI_2 + angle);
    markerRotationAnimation.duration = animationDuration;
    
    //Round path
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint center = CGPointMake(self.frame.size.width / 2, self.bounds.size.height - OutLineOffset);
    CGFloat radius = center.x - marker.frame.size.width / 2;
    
    [path addArcWithCenter:center radius:radius startAngle:M_PI endAngle:M_PI + angle clockwise:YES];
    
    //Round path animation
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:AnimationKeyPosition];
    pathAnimation.path = path.CGPath;
    pathAnimation.duration = animationDuration;
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = animationDuration;
    animationGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animationGroup.animations = @[markerRotationAnimation, pathAnimation];
    
    [marker.layer addAnimation:animationGroup forKey:@"animations"];
    
    //Save state
    CGPoint pointToMark = CGPointMake(center.x - radius * cosf(angle),
                                      center.y - radius * sinf(angle));
    marker.layer.position = pointToMark;
    marker.layer.transform = CATransform3DMakeRotation(3 * M_PI_2 + angle, 0, 0.0, 1.0);
}

@end
